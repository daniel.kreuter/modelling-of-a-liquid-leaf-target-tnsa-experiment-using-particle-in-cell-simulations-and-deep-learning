# Modelling of a Liquid Leaf Target TNSA Experiment using Particle-In-Cell Simulations and Deep Learning

This is a repository containing code files used in Daniel Kreuter's Master Thesis from 2021.

The Particle-In-Cell code used was [Smilei](https://github.com/SmileiPIC/Smilei).

Namelists for running the code are included in the Python-Files directory. The simulation results are stored locally as their filesize is quite large.

The files used for running the simulations on [Virgo](https://hpc.gsi.de/virgo) as well as training the machine learning models are contained in the "VirgoSimsScripts" and "Virgo2D" directories.

A .def-file for building a Singularity container image with Smilei installed is also included. The most recent version of the container on the Sylabs-Cloud can be found [here](https://cloud.sylabs.io/library/_container/60bf6d9cefa7337ba36161de).
