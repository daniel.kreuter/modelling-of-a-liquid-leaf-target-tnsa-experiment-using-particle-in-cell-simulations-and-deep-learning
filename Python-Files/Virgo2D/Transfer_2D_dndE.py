#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov  8 11:03:13 2021

@author: dkreuter
"""

# ATTEMPT AT TRANSFER LEARNING FOR THE CONTINUOUS MODEL

# ----------------------
# IMPORT MODULES & DATA
# ----------------------

from tensorflow import keras
import numpy as np
from sklearn.model_selection import train_test_split
from keras_tuner import BayesianOptimization, RandomSearch
import datetime

# Load Data
dataH = np.loadtxt("inputoutput-H.csv", delimiter=",", usecols=np.arange(0, 114))

data = dataH

x = np.zeros((100 * data.shape[0], 8))

for i in range(data.shape[0]):
    for j in range(100):
        x[int(100 * i + j)] = np.concatenate(
            (np.array([data[i, -1] * j / 100]), data[i, :7])
        )

# Normalisation by maximum Parameter
x[:, 1] = x[:, 1] / 50
x[:, 2] = x[:, 2] / 20e-6
x[:, 3] = x[:, 3] / 150e-15
x[:, 5] = x[:, 5] / 85
x[:, 6] = x[:, 6] / 1100e-9
x[:, 7] = x[:, 7] / 3e-6

# preparing y similarly
y = np.zeros((100 * data.shape[0], 1))

for i in range(data.shape[0]):
    for j in range(13, 113):
        y[int(100 * i + j - 13)] = data[i, j]

x[x == -np.inf] = 0
y[y == -np.inf] = 0

# Split Train, Test (Validation split is done later during training)
x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.1)

print(
    "Available data: "
    + str(len(x))
    + " data points. ("
    + str(0.9 * len(x_train))
    + " training, "
    + str(0.1 * len(x_train))
    + " validation, "
    + str(len(x_test))
    + " testing)"
)

# -----------------------------------------------------
# LOAD PREVIOUS MODEL AND BUILD NEW MODEL WITH WEIGHTS
# -----------------------------------------------------

H_model = keras.models.load_model("../VirgoSimsScripts/best_model_dndE_CloudTuner.h5")
H_weights = H_model.get_weights()

pretrained = keras.Model(
    H_model.inputs, H_model.layers[-2].input, name="pretrained_model"
)
pretrained.trainable = False


inputs = keras.Input(shape=x_train.shape[1])
layer = pretrained(inputs, training=False)
layer = keras.layers.Dense(320, activation="relu", name="added_layer")(layer)
outputs = keras.layers.Dense(y_train.shape[1], name="output_layer")(layer)

model = keras.Model(inputs, outputs)

model.layers[-1].set_weights(H_weights[12:])
model.layers[-2].set_weights(H_weights[10:12])

# ----------------------
# TRAIN MODEL
# ----------------------

l1 = keras.regularizers.l1(l1=1e-9)
l2 = keras.regularizers.l2(l2=4e-6)
l1l2 = keras.regularizers.l1_l2(l1=0.0002, l2=1.1e-7)
savedmodel = "best_model_2D_dndE_fullretrain_lowlr.h5"

log_dir = "logs/fit/" + datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
tensorboard_callback = keras.callbacks.TensorBoard(log_dir=log_dir)

callbacks = [
    # keras.callbacks.ModelCheckpoint(
    #     savedmodel, save_best_only=True, monitor="val_loss"
    # ),
    keras.callbacks.ReduceLROnPlateau(
        monitor="val_loss", factor=0.5, patience=20, min_lr=1e-6
    ),
    keras.callbacks.EarlyStopping(monitor="val_loss", patience=50, verbose=1),
    # tensorboard_callback
]

epochs = 1000000
batch_size = 2**6


model.compile(
    optimizer=keras.optimizers.Adam(1e-4),
    loss="mse",
    metrics=[keras.metrics.MeanSquaredError()],
)

model.summary()


model.fit(
    x_train,
    y_train,
    batch_size=batch_size,
    epochs=epochs,
    callbacks=callbacks,
    validation_split=0.1,
    verbose=1,
)


pretrained.trainable = True
# layer.trainable = True


print("-------------FIRST FIT DONE-------------")

callbacks_full = [
    keras.callbacks.ModelCheckpoint(
        savedmodel, save_best_only=True, monitor="val_loss"
    ),
    keras.callbacks.ReduceLROnPlateau(
        monitor="val_loss", factor=0.5, patience=20, min_lr=1e-7
    ),
    keras.callbacks.EarlyStopping(monitor="val_loss", patience=50, verbose=1),
    # tensorboard_callback
]

model.compile(
    optimizer=keras.optimizers.Adam(1e-6),
    loss="mse",
    metrics=[keras.metrics.MeanSquaredError()],
)

model.summary()


history = model.fit(
    x_train,
    y_train,
    batch_size=batch_size,
    epochs=epochs,
    callbacks=callbacks_full,
    validation_split=0.1,
    verbose=1,
)

# ----------------------
# TEST MODEL
# ----------------------

model = keras.models.load_model(savedmodel)

test_loss, test_acc = model.evaluate(x_test, y_test)

print("Test mean squared error", test_acc)
print("Test loss", test_loss)
