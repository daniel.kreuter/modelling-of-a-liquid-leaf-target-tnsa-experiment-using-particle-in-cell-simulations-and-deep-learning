# -*- coding: utf-8 -*-
"""
Created on Fri Nov 19 10:58:56 2021

@author: dkreuter
"""

# ATTEMPT OF TRANSFER LEARNING ON THE MAXIMUM ENERGY MODEL

# ----------------------
# IMPORT MODULES & DATA
# ----------------------

from tensorflow import keras
import tensorflow as tf
import numpy as np

# import matplotlib.pyplot as plt
# from skimage.measure import block_reduce
from sklearn.model_selection import train_test_split
from keras_tuner import BayesianOptimization
import datetime

# Load Data

dataH = np.loadtxt("inputoutput-H.csv", delimiter=",", usecols=np.arange(0, 114))
dataH = dataH[dataH[:, 10] > 1.0]

data = dataH

# x: List of physical parameters
x = data[:, :7]

# Normalisation by maximum Parameter
x[:, 0] = x[:, 0] / 50
x[:, 1] = x[:, 1] / 20e-6
x[:, 2] = x[:, 2] / 150e-15
x[:, 4] = x[:, 4] / 85
x[:, 5] = x[:, 5] / 1100e-9
x[:, 6] = x[:, 6] / 3e-6

# y: List of maximum energies in MeV
y = np.array([data[:, -1]]).T

y[y == -np.inf] = 0

# Split Train, Test (Validation split is done later during training)
x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.1)

print(
    "Available data: "
    + str(len(x))
    + " data points. ("
    + str(0.9 * len(x_train))
    + " training, "
    + str(0.1 * len(x_train))
    + " validation, "
    + str(len(x_test))
    + " testing)"
)

# -----------------------------------------------------
# LOAD PREVIOUS MODEL AND BUILD NEW MODEL WITH WEIGHTS
# -----------------------------------------------------

l1 = keras.regularizers.l1(l1=1e-9)
l2 = keras.regularizers.l2(l2=4e-6)
l1l2 = keras.regularizers.l1_l2(l1=2.3e-4, l2=1.1e-7)
savedmodel = "best_model_2D_Emax_lastlayer.h5"

log_dir = "logs/Emax/" + datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
tensorboard_callback = keras.callbacks.TensorBoard(log_dir=log_dir)

callbacks = [
    # keras.callbacks.ModelCheckpoint(
    #     savedmodel, save_best_only=True, monitor="val_loss"
    # ),
    keras.callbacks.ReduceLROnPlateau(
        monitor="val_loss", factor=0.5, patience=20, min_lr=1e-7
    ),
    keras.callbacks.EarlyStopping(monitor="val_loss", patience=50, verbose=1),
    # tensorboard_callback
]

H_model = keras.models.load_model("../VirgoSimsScripts/best_tuned_model_Emax.h5")

H_weights = H_model.get_weights()

pretrained = keras.Model(
    H_model.inputs, H_model.layers[-2].input, name="pretrained_model"
)
pretrained.trainable = False

# H_model.summary()

inputs = keras.Input(shape=x_train.shape[1])
layer = pretrained(inputs, training=False)
layer = keras.layers.Dense(
    32, activation="relu", kernel_regularizer=l1l2, name="added_layer"
)(layer)
outputs = keras.layers.Dense(y_train.shape[1], name="output_layer")(layer)

model = keras.Model(inputs, outputs)
model.layers[-1].set_weights(H_weights[16:])
model.layers[-2].set_weights(H_weights[14:16])

# ----------------------
# TRAIN MODEL
# ----------------------

epochs = 1000000
batch_size = 64


model.compile(
    optimizer=keras.optimizers.Adam(1e-4),
    loss="mse",
    metrics=[keras.metrics.MeanSquaredError()],
)

model.summary()


model.fit(
    x_train,
    y_train,
    batch_size=batch_size,
    epochs=epochs,
    callbacks=callbacks,
    validation_split=0.1,
    verbose=1,
)


pretrained.trainable = True
# layer.trainable = True


print("-------------FIRST FIT DONE-------------")

callbacks_full = [
    keras.callbacks.ModelCheckpoint(
        savedmodel, save_best_only=True, monitor="val_loss"
    ),
    keras.callbacks.ReduceLROnPlateau(
        monitor="val_loss", factor=0.5, patience=20, min_lr=1e-7
    ),
    keras.callbacks.EarlyStopping(monitor="val_loss", patience=50, verbose=1),
    # tensorboard_callback
]

model.compile(
    optimizer=keras.optimizers.Adam(1e-6),
    loss="mse",
    metrics=[keras.metrics.MeanSquaredError()],
)

model.summary()


history = model.fit(
    x_train,
    y_train,
    batch_size=batch_size,
    epochs=epochs,
    callbacks=callbacks_full,
    validation_split=0.1,
    verbose=1,
)

# ----------------------
# TEST MODEL
# ----------------------

# model = keras.models.load_model(savedmodel)

test_loss, test_acc = model.evaluate(x_test, y_test)

print("Test mean squared error", test_acc)
print("Test loss", test_loss)
