# -*- coding: utf-8 -*-
"""
Created on Fri Apr  9 13:44:03 2021

@author: dkreuter
"""

# --------------
# IMPORT MODULES
# --------------

import numpy as np
import os
import subprocess


# Name of the SMILEI namelist
Exec = "1dLorentzBoostTNSA.py"
# List of angles to be scanned
angles = np.arange(0, 90, 5)

for theta in angles:
    # Step1: Specify path and make directory for the new angle
    DirPath = str(theta) + "deg"
    if not os.path.exists(DirPath):
        os.makedirs(DirPath)

    # Step2: Copy input namelist into the new folder
    bashCopy = "cp " + Exec + " ./" + DirPath + "/"
    process1 = subprocess.Popen(bashCopy, shell=True)
    process1.wait()

    # Step3: Run script with proper parameters and in proper directory
    namelist_path = DirPath + "/" + Exec
    with open(namelist_path, "r+") as file:
        lines = file.readlines()
        lines.insert(
            60,
            "theta       = "
            + str(theta)
            + ". * (np.pi/180.)  # ANGLE WRITTEN BY SYSTEM SCRIPT",
        )
        file.seek(0)
        file.writelines(lines)

    bashExec = "~/Smilei/smilei " + Exec
    process2 = subprocess.Popen(bashExec, shell=True, cwd=DirPath)
    process2.wait()
