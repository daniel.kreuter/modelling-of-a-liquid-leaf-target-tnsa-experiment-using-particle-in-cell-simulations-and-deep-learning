#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 16 15:38:07 2020

@author: dkreuter
"""

# -------------------
# IMPORT MODULES
# -------------------
import numpy as np
import scipy.constants as const

# -------------------
# EXTRA FUNCTIONS
# -------------------


def LiquidJetDensity(
    n0=1.0, start=0.0, thickness=1.0, CutOff=0.0, lscale=0.1, njet=1.0, L=1.0
):
    # Liquid Jet Target density profile including pre-plasma
    rjet = 0.5 * thickness

    def f(x):
        if x < start - CutOff:
            return 0.0
        elif x < start:
            return n0 / (1.0 + np.exp(-(x - start) / lscale)) + njet * (
                rjet / np.abs(x - start - 0.5 * thickness)
            ) ** 2.0 * L / np.sqrt(
                np.abs(x - start - 0.5 * thickness) ** 2.0 + L**2.0
            )
        elif x < start + thickness:
            return n0
        elif x < start + thickness + CutOff:
            return n0 / (1.0 + np.exp((x - start - thickness) / lscale)) + njet * (
                rjet / np.abs(x - start - 0.5 * thickness)
            ) ** 2.0 * L / np.sqrt(
                np.abs(x - start - 0.5 * thickness) ** 2.0 + L**2.0
            )
        else:
            return 0.0

    return f


""" def Te(a=1., C=10**6, ne=1e28, tpre=2e-9):
    # Electron temperature after Prepulse (as suggested by Esirkepov et al., 2014)
    return (5.1*np.sqrt(a**2./C) + 1.3*(ne/1e27 * tpre/1e-9 * a**2./C)**(2./5.)) * 1e5 * e

def ScaleLength(T=1.6e-16, Zstar=1., A=1., tL=100e-15):
    # Scale-length according to Gibbon book
    return 3.*np.sqrt(T/(1000.*e)) * np.sqrt(Zstar/A) * tL/1e-15 * 1e-10

def kInf(theta0=0., kMax=0.1, a=1.):
    # Absorption coefficient kappa_inf for p-polarised light as suggested by Gibbon et al., 2012)
    F = kMax/((1.+np.sqrt(1.-kMax))**2.) * (np.tan(theta0)/a + 1.)
    return 4. * F/((1.+F)**2.) """


def polAngle(pol="s"):
    # Getting polarisation angle for SMILEI
    if pol == "p":
        ang = 0.0 * np.pi / 180.0
    elif pol == "s":
        ang = 90.0 * np.pi / 180.0
    return ang


def Thot(a0):
    # Beg's Scaling Law for the hot electron temperature kT (returned in Joules)
    return 215.0 * (1.37 * a0**2.0) ** (1.0 / 3.0) * 1e3 * e


# -------------------
# MY PYTHON VARIABLES
# -------------------


theta = 82.0 * (np.pi / 180.0)  # ANGLE WRITTEN BY SYSTEM SCRIPT


gamma0 = 1.0 / np.cos(theta)  # Lorentz factor for the boosted system

epsilon0 = const.epsilon_0  # Vaccuum permittivity
me = const.m_e  # Electron mass
mp = const.m_p  # Proton mass
e = const.e  # Elemantary charge
c0 = const.c  # Speed of light
lambdal = 520e-9 * gamma0  # Laser wavelength
omegal = 2.0 * np.pi * c0 / lambdal  # Laser radial frequency

refl = c0 / omegal  # SMILEI reference length c/omegaL
reft = gamma0 / omegal  # SMILEI reference time 1/omegaL
refn = (
    epsilon0 * me * (omegal**2.0) / (e**2.0) / gamma0
)  # SMILEI reference particle density n_c
refk = me * c0**2.0  # SMILEI reference energy m_e c^2
refq = e  # SMILEI reference charge e
refm = me  # SMILEI reference mass m_e
refv = c0  # SMILEI reference velocity c

# Note that transformations have already been added into the reference quantities

# Using the reference quantities we can write every parameter in SI-units (e.g. 1E-6/refl = 1 micron)

frontdist = 5e-6 / refl  # Distance before plasma
backdist = 13e-6 / refl  # Distance after plasma
plasmathick = 2e-6 / refl  # Target thickness
length0 = plasmathick + frontdist + backdist  # Box length
# tstep       = 0.1E-15/reft                      # Time step length
tl = 30e-15 / reft  # Laser pulse duration
partden = 6.68e28 / refn  # Particle density
jetden = 1.62e23 / refn  # n(r_jet)

aL = 5.0  # dim.less laser amplitude
temph = 10e3 * e / refk  # Initial electron temperature

n_part = 500.0  # Number of particles per cell
output_every = 10.0  # Number of timesteps between diag call

cfl = 0.98  # CFL-number

omegap0 = np.sqrt(
    (partden * refn * e**2.0) / (me * epsilon0 * np.sqrt(1.0 + (aL**2.0) / 2.0))
)  # Initial plasma-freq in SI
LambdaDebye = np.sqrt(
    (epsilon0 * temph * refk) / (partden * refn * e**2.0)
)  # Debye-length in SI
LambdaDebyeSmilei = LambdaDebye / refl  # Debye-length in Smilei units
dskin = c0 / omegap0  # Skin depth
# lscale      = ScaleLength(T=temph*refk, Zstar=1., A=1., tL=1e-12)/(refl)

cs = np.sqrt(Thot(aL) / (mp * gamma0))  # Ion sound speed


xstep = LambdaDebyeSmilei * 0.5  # Cell length
length = np.ceil((length0 / xstep) / 32.0) * xstep * 32.0  # Grid-length for 32 patches
ncells = length / xstep  # Number of Cells in Simulation
tacc = (
    tl * reft + frontdist * refl / c0 + tl * reft + plasmathick * refl / cs
)  # Acceleration time for TNSA protons according to Zsolt 2013
Tsim = (tacc + 5e-15) / reft  # Simulation time
# Tsim        = 600e-15/reft


# --------------------------------------
# SMILEI's VARIABLES (DEFINED IN BLOCKS)
# --------------------------------------

Main(
    geometry="1Dcartesian",
    interpolation_order=2.0,
    simulation_time=Tsim,
    # timestep = tstep,
    grid_length=[length],
    cell_length=[xstep],
    timestep_over_CFL=cfl,
    number_of_patches=[32.0],
    EM_boundary_conditions=[["silver-muller"]],
)

LoadBalancing(  # not sure if this is needed
    initial_balance=True, every=150.0, cell_load=1.0, frozen_particle_load=0.1
)

LaserPlanar1D(
    box_side="xmin",
    a0=aL,
    omega=1.0,
    polarization_phi=polAngle(pol="p"),
    ellipticity=0.0,
    time_envelope=tgaussian(duration=2 * tl, fwhm=tl, center=tl),
)

Species(
    name="ions",
    position_initialization="regular",
    momentum_initialization="cold",
    particles_per_cell=n_part,
    # mass = gamma0**2 * mp/refm,
    # charge = gamma0**2 * e/refq,
    mass=mp / refm,
    charge=e / refq,
    mean_velocity=[0.0, -c0 * np.sin(theta) / refv, 0.0],
    number_density=LiquidJetDensity(
        n0=partden,
        start=frontdist,
        thickness=plasmathick,
        CutOff=4e-6 / refl,
        lscale=0.4e-6 / refl,
        njet=jetden,
        L=0.03 / refl,
    ),
    # time_frozen = 0.,
    boundary_conditions=[
        ["remove"],
    ],
)

Species(
    name="electrons",
    position_initialization="regular",
    momentum_initialization="maxwell-juettner",
    particles_per_cell=n_part,
    # mass = gamma0**2 * me/refm,
    # charge = gamma0**2 * -e/refq,
    mass=me / refm,
    charge=-e / refq,
    mean_velocity=[0.0, -c0 * np.sin(theta) / refv, 0.0],
    temperature=[temph],
    number_density=LiquidJetDensity(
        n0=partden,
        start=frontdist,
        thickness=plasmathick,
        CutOff=4e-6 / refl,
        lscale=0.4e-6 / refl,
        njet=jetden,
        L=0.03 / refl,
    ),
    boundary_conditions=[
        ["remove"],
    ],
)

# --------------------------------------
# DIAGNOSTICS
# --------------------------------------


def timetostep(t):
    return t / (cfl * xstep)  # time in SMILEI -> timestep in SMILEI


DiagScalar(
    every=1.0,
    # precision = 10
)

DiagTrackParticles(
    species="electrons",
    every=[timetostep(tacc / reft), timetostep(tacc / reft), 0.0],
    #    flush_every = 100,
    #    filter = my_filter,
    attributes=["x", "px", "py", "pz", "w"],
)

DiagTrackParticles(
    species="ions",
    every=[timetostep(tacc / reft), timetostep(tacc / reft), 0.0],
    #    flush_every = 100,
    #    filter = my_filter,
    attributes=["x", "px", "py", "pz", "w"],
)

""" DiagParticleBinning(
    deposited_quantity = "weight",
    every = 10.,
    #time_average = 1,
    species = ["ions"],
    axes = [ ["x",    0.,    length,    100.] ,
            ["vx", 0., 1., 100., "edge_inclusive"]]
)

DiagParticleBinning(
    deposited_quantity = "weight",
    every = 10.,
    #time_average = 1,
    species = ["electrons"],
    axes = [ ["x",    0.,    length,    100.] ,
            ["vx", 0., 1., 100., "edge_inclusive"]]
)

DiagFields(
    #name = "my field diag",
    every = 10,
    time_average = 2,
    fields = ["Ex", "Ey"],
    #subgrid = None
) """
