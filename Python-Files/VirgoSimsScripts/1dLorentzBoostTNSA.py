#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 16 15:38:07 2020

@author: dkreuter
"""

# GENERAL 1D LORENTZ BOOST SIMULATION OF WATER LEAF JET TARGET

# -------------------
# IMPORT MODULES
# -------------------
import numpy as np
import scipy.constants as const

# -------------------
# EXTRA FUNCTIONS
# -------------------


def LiquidJetDensity(
    n0=1.0, start=0.0, thickness=1.0, CutOff=0.0, lscale=0.1, njet=1.0, L=1.0, ratio=1.0
):
    # Liquid Jet Target density profile including pre-plasma and post-plasma skirt; ratio included for multiple species
    rjet = 0.5 * thickness

    def f(x):
        if x < start - CutOff:
            return 0.0
        elif x < start:
            return ratio * (
                n0 / (1.0 + np.exp(-(x - start) / lscale))
                + njet
                * (rjet / np.abs(x - start - 0.5 * thickness)) ** 2.0
                * L
                / np.sqrt(np.abs(x - start - 0.5 * thickness) ** 2.0 + L**2.0)
            )
        elif x < start + thickness:
            return ratio * n0
        elif x < start + thickness + CutOff:
            return ratio * (
                n0 / (1.0 + np.exp((x - start - thickness) / lscale))
                + njet
                * (rjet / np.abs(x - start - 0.5 * thickness)) ** 2.0
                * L
                / np.sqrt(np.abs(x - start - 0.5 * thickness) ** 2.0 + L**2.0)
            )
        else:
            return 0.0

    return f


def polAngle(pol=0.0):
    # Getting polarisation angle for SMILEI
    if pol == 1.0:
        ang = 0.0 * np.pi / 180.0
    elif pol == 0.0:
        ang = 90.0 * np.pi / 180.0
    return ang


def Thot(a0):
    # Beg's Scaling Law for the hot electron temperature kT (returned in Joules)
    return 215.0 * (1.37 * a0**2.0) ** (1.0 / 3.0) * 1e3 * e


# -------------------
# MY PYTHON VARIABLES
# -------------------


theta = 0.0 * (np.pi / 180.0)  # Laser incidence angle (default)


gamma0 = 1.0 / np.cos(theta)  # Lorentz factor for the boosted system

epsilon0 = const.epsilon_0  # Vaccuum permittivity
me = const.m_e  # Electron mass
mp = const.m_p  # Proton mass
e = const.e  # Elemantary charge
c0 = const.c  # Speed of light


lambdal = 600e-9 * gamma0  # Laser wavelength


omegal = 2.0 * np.pi * c0 / lambdal  # Laser radial frequency

refl = c0 / omegal  # SMILEI reference length c/omegaL
reft = gamma0 / omegal  # SMILEI reference time 1/omegaL
refn = (
    epsilon0 * me * (omegal**2.0) / (e**2.0) / gamma0
)  # SMILEI reference particle density n_c
refk = me * c0**2.0  # SMILEI reference energy m_e c^2
refq = e  # SMILEI reference charge e
refm = me  # SMILEI reference mass m_e
refv = c0  # SMILEI reference velocity c

# Note that transformations have already been added into the reference quantities

# Using the reference quantities we can write every parameter in SI-units (e.g. 1E-6/refl = 1 micron)

frontdist = 5e-6 / refl  # Distance before plasma
backdist = 40e-6 / refl  # Distance after plasma


plasmathick = 2e-6 / refl  # Target thickness


length0 = plasmathick + frontdist + backdist  # Box length
# tstep       = 0.1E-15/reft                      # Time step length


tl = 30e-15 / reft  # Laser pulse duration


partden = 6.68e28 / refn  # Particle density
jetden = 1.62e23 / refn  # n(r_jet)


aL = 20.0  # dim.less laser amplitude


laserpolid = 0.0  # Laser polarisation from file


temph = 10e3 * e / refk  # Initial electron temperature

n_part = 800.0  # Number of particles per cell
output_every = 100.0  # Number of timesteps between diag call

cfl = 0.98  # CFL-number

omegap0 = np.sqrt(
    (partden * refn * e**2.0) / (me * epsilon0 * np.sqrt(1.0 + (aL**2.0) / 2.0))
)  # Initial plasma-freq in SI
LambdaDebye = np.sqrt(
    (epsilon0 * temph * refk) / (partden * refn * e**2.0)
)  # Debye-length in SI
LambdaDebyeSmilei = LambdaDebye / refl  # Debye-length in Smilei units
dskin = c0 / omegap0  # Skin depth
# lscale      = ScaleLength(T=temph*refk, Zstar=1., A=1., tL=1e-12)/(refl)

cs = np.sqrt(Thot(aL) / (mp * gamma0))  # Ion sound speed (INSERT LIGHTEST ION MASS)


xstep = LambdaDebyeSmilei * 1.0
n_patches = 2.0**10.0  # Number of patches
length = (
    np.ceil((length0 / xstep) / n_patches) * xstep * n_patches
)  # Grid-length for  n_patches
ncells = length / xstep  # Number of Cells in Simulation
tacc = (
    tl * reft + frontdist * refl / c0 + tl * reft + plasmathick * refl / cs
)  # Acceleration time for TNSA protons according to Zsolt 2013
Tsim = (tacc + 5e-15) / reft  # Simulation time
# Tsim        = 500e-15/reft


# --------------------------------------
# SMILEI's VARIABLES (DEFINED IN BLOCKS)
# --------------------------------------

Main(
    geometry="1Dcartesian",
    interpolation_order=4,
    simulation_time=Tsim,
    # timestep = tstep,
    grid_length=[length],
    cell_length=[xstep],
    timestep_over_CFL=cfl,
    number_of_patches=[n_patches],
    EM_boundary_conditions=[["silver-muller"]],
    random_seed=12345,
    print_every=2000.0,
)

LoadBalancing(
    initial_balance=True, every=500.0, cell_load=1.0, frozen_particle_load=0.1
)

LaserPlanar1D(
    box_side="xmin",
    a0=aL,
    omega=1.0,
    polarization_phi=polAngle(pol=laserpolid),
    ellipticity=0.0,
    time_envelope=tgaussian(duration=2 * tl, fwhm=tl, center=tl),
)

# Water is fully ionised; since oxygen has 8 electrons and the hydrogens have 2 together,
# hydrogens have 2/10 density and oxygen has 1/10 density in order to maintain neutrality

Species(
    name="hydrogen",
    position_initialization="regular",
    momentum_initialization="cold",
    particles_per_cell=n_part,
    mass=mp / refm,
    charge=e / refq,
    mean_velocity=[0.0, -c0 * np.sin(theta) / refv, 0.0],
    number_density=LiquidJetDensity(
        n0=partden,
        start=frontdist,
        thickness=plasmathick,
        CutOff=4e-6 / refl,
        lscale=0.4e-6 / refl,
        njet=jetden,
        L=0.03 / refl,
        ratio=1.0 / 5.0,
    ),
    # time_frozen = 0.,
    boundary_conditions=[
        ["remove"],
    ],
)

Species(
    name="oxygen",
    position_initialization="regular",
    momentum_initialization="cold",
    particles_per_cell=n_part,
    mass=2.655967247e-26 / refm,
    charge=8.0 * e / refq,
    mean_velocity=[0.0, -c0 * np.sin(theta) / refv, 0.0],
    number_density=LiquidJetDensity(
        n0=partden,
        start=frontdist,
        thickness=plasmathick,
        CutOff=4e-6 / refl,
        lscale=0.4e-6 / refl,
        njet=jetden,
        L=0.03 / refl,
        ratio=1.0 / 10.0,
    ),
    # time_frozen = 0.,
    boundary_conditions=[
        ["remove"],
    ],
)

Species(
    name="electrons",
    position_initialization="regular",
    momentum_initialization="maxwell-juettner",
    particles_per_cell=n_part,
    mass=me / refm,
    charge=-e / refq,
    mean_velocity=[0.0, -c0 * np.sin(theta) / refv, 0.0],
    temperature=[temph],
    number_density=LiquidJetDensity(
        n0=partden,
        start=frontdist,
        thickness=plasmathick,
        CutOff=4e-6 / refl,
        lscale=0.4e-6 / refl,
        njet=jetden,
        L=0.03 / refl,
        ratio=1.0,
    ),
    boundary_conditions=[
        ["remove"],
    ],
)

# --------------------------------------
# DIAGNOSTICS
# --------------------------------------


def timetostep(t):
    return t / (cfl * xstep)  # time in SMILEI -> timestep in SMILEI


DiagTrackParticles(
    species="electrons",
    every=[timetostep(tacc / reft), timetostep(tacc / reft), 0.0],
    #    flush_every = 100,
    #    filter = my_filter,
    attributes=["x", "px", "py", "pz", "w"],
)

DiagTrackParticles(
    species="hydrogen",
    every=[timetostep(tacc / reft), timetostep(tacc / reft), 0.0],
    #    flush_every = 100,
    #    filter = my_filter,
    attributes=["x", "px", "py", "pz", "w"],
)

DiagTrackParticles(
    species="oxygen",
    every=[timetostep(tacc / reft), timetostep(tacc / reft), 0.0],
    #    flush_every = 100,
    #    filter = my_filter,
    attributes=["x", "px", "py", "pz", "w"],
)

"""
DiagParticleBinning(
    #name = "my binning",
    deposited_quantity = "weight",
    every = 100,
    time_average = 1,
    species = ["hydrogen"],
    axes = [
        ["ekin", 0, 85e6*e/refk, 100, "edge_inclusive"]
    ]
)
"""
