# -*- coding: utf-8 -*-
"""
Created on Thu Sep  2 11:31:36 2021

@author: dkreuter
"""

# FULLY CONNECTED NETWORK PREDICTING E_max FOR HYDROGEN

# ----------------------
# IMPORT MODULES & DATA
# ----------------------

from tensorflow import keras
import numpy as np
from sklearn.model_selection import train_test_split
from keras_tuner import BayesianOptimization
import datetime

# Load Data

dataH = np.loadtxt("inputoutput-H.csv", delimiter=",", usecols=np.arange(0, 114))
dataH = dataH[dataH[:, 10] > 1.0]

data = dataH
data = data[data[:, 10] > 10.0]

# x: List of physical parameters
x = data[:, :7]

# Normalisation by maximum Parameter
x[:, 0] = x[:, 0] / 50
x[:, 1] = x[:, 1] / 20e-6
x[:, 2] = x[:, 2] / 150e-15
x[:, 4] = x[:, 4] / 85
x[:, 5] = x[:, 5] / 1100e-9
x[:, 6] = x[:, 6] / 3e-6

# y: List of normalised max energies in MeV
y = np.array([data[:, -1]]).T

y[y == -np.inf] = 0

# Split Train, Test (Validation split is done later during training)
x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.1)

print(
    "Available data: "
    + str(len(x))
    + " data points. ("
    + str(0.9 * len(x_train))
    + " training, "
    + str(0.1 * len(x_train))
    + " validation, "
    + str(len(x_test))
    + " testing)"
)

# ----------------------
# DEFINE MODEL
# ----------------------

l1 = keras.regularizers.l1(l1=1e-9)
l2 = keras.regularizers.l2(l2=4e-6)
l1l2 = keras.regularizers.l1_l2(l1=0.0002, l2=1.1e-7)

log_dir = "logs/fit/" + datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
tensorboard_callback = keras.callbacks.TensorBoard(log_dir=log_dir, histogram_freq=1)

callbacks = [
    # keras.callbacks.ModelCheckpoint(
    #     savedmodel, save_best_only=True, monitor="val_loss"
    # ),
    # tensorboard_callback,
    keras.callbacks.ReduceLROnPlateau(
        monitor="val_loss", factor=0.5, patience=20, min_lr=0.0001
    ),
    keras.callbacks.EarlyStopping(monitor="val_loss", patience=30, verbose=1),
]

epochs = 2000
batch_size = 2**10

# ----------------------
# TUNER MODEL
# ----------------------


def build_model(hp):
    l1_hype = hp.Float("l1", min_value=1e-12, max_value=1.0, sampling="log")
    l2_hype = hp.Float("l2", min_value=1e-12, max_value=1.0, sampling="log")
    reg = keras.regularizers.l1_l2(l1=l1_hype, l2=l2_hype)
    input_layer = keras.layers.Input(x_train.shape[1])
    layer = input_layer
    for i in range(hp.Int("num_layers", min_value=10, max_value=20)):
        layer = keras.layers.Dense(
            units=hp.Int("units_" + str(i), min_value=32, max_value=512, step=32),
            activation="relu",
            kernel_regularizer=reg,
        )(layer)

    output_layer = keras.layers.Dense(y_train.shape[1])(layer)

    model = keras.models.Model(inputs=input_layer, outputs=output_layer)
    model.compile(
        optimizer="adam",
        loss=keras.losses.MeanSquaredError(),
        metrics=[keras.metrics.MeanSquaredError()],
    )
    model.summary()
    return model


tuner = BayesianOptimization(
    build_model,
    objective="val_mean_squared_error",
    max_trials=100,
    executions_per_trial=2,
    overwrite=False,
    directory=".",
    project_name="Emax_Tuner_L1L2",
)

# tuner.search_space_summary()

# tuner.search(x_train, y_train, batch_size=batch_size, epochs=epochs,
# callbacks=callbacks, validation_split=0.1)

tuner.results_summary()

# best_model = tuner.get_best_models()[0]

# best_model.save('best_tuned_model_Emax')
