# -*- coding: utf-8 -*-
"""
Created on Mon Dec 21 10:35:36 2020

@author: dkreu
"""

# Python-script of collection of useful functions for analysis of Smilei Data
# Before: run S=happi.Open([file location])


def convl(array):  # SMILEI length -> µm
    return array * refl * 1.0e6


def convt(array):
    return array * reft * 1.0e15  # SMILEI time -> fs


def convn(array):
    return array * refn * 1.0e-6  # SMILEI particle density -> 1/cm³


def conve(array):
    return array * refk / e * 1.0e-6  # SMILEI energy -> MeV


def tau(t):
    return (
        np.sqrt((S.namelist.partden * refn * e**2) / (mp * epsilon0))
        * t
        * 1.0e-15
        / np.sqrt(2 * np.exp(1))
    )  # Mora model tau (for t in fs)


def timetostep(t):
    return (
        t * c0 / (0.98 * S.namelist.xstep * refl)
    )  # time in SI -> timestep in SMILEI (CFL = 0.98)


def nit(t):  # normalised density at timestep
    return np.array(ni.getData(timestep=t))[0]


def net(t):
    return np.array(ne.getData(timestep=t))[0]


def zfront(
    x, debye0
):  # Fit of z_front [Mora] with the parameter initial Debye-length (the position of the target backside is added)
    return 2 * np.sqrt(2 * np.exp(1)) * debye0 * (
        tau(x) * np.log(tau(x) + np.sqrt(1 + tau(x) ** 2))
        - np.sqrt(1 + tau(x) ** 2)
        + 1
    ) + convl(S.namelist.frontdist + S.namelist.plasmathick)


def pfront(
    x, cs
):  # v_front * proton mass fit in units m_e*c [Mora]. Paramters: sound speed cs
    return (mp * 2 * cs * np.log(tau(x) + np.sqrt(1 + tau(x) ** 2))) / (me * c0)


def ukinit(t):  # Energy in eV * m^-3
    return np.array(ukini.getData(timestep=t))[0]  # *refk*refn/e


def ukinet(t):
    return np.array(ukine.getData(timestep=t))[0]  # *refk*refn/e


def espec(t):
    return np.array(enspeci.getData(timestep=t))[0]


def predespec(
    E,
):  # dN/dE analytical for E in J and t in seconds (Eq. 22 and 9 from TNSA Roth paper) (progress: c_s*t out for unit reasons)
    return (
        (S.namelist.partden * refn)
        / np.sqrt(2 * 4.10479e-13 * E)
        * np.exp(-np.sqrt((2 * E) / (4.10479e-13)))
    )


def rightestnonzero(a):
    """
    Find the index of the right-most entry (i.e. the first when counting from last to first) that isn't zero of a
    """
    for i in range(-1, -a.shape[0] + 1, -1):
        if a[i] != 0.0:
            break
    return i
